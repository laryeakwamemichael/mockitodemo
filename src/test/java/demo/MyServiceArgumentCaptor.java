package demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;

public class MyServiceArgumentCaptor {

    @InjectMocks
    private MyService underTest;

    @Mock
    private ExternalService externalService;

    @Captor
    ArgumentCaptor<String> argumentCaptor;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void itShouldValidateID(){
        String id = "10";

        when(externalService.getValidationData(anyString())).thenReturn("some data");

        Boolean result = underTest.validate(id);

        then(externalService).should().getValidationData(argumentCaptor.capture());

        String argumentCaptorValue = argumentCaptor.getValue();

        assertEquals("10", argumentCaptorValue);


    }
}
