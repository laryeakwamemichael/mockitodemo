package demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

public class MyServiceStaticMockTest {

    private MyService underTest;

    private ExternalService externalService;

    @BeforeEach
    void setUp(){
        externalService = Mockito.mock(ExternalService.class);
        underTest = new MyService(externalService);
    }

    @Test
    void itShouldValidateID(){
        String id = "10";

        when(externalService.getValidationData(id)).thenReturn("some data");

        Boolean result = underTest.validate(id);

        assertTrue(result);


    }
}
